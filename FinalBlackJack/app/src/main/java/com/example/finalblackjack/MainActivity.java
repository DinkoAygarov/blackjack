package com.example.finalblackjack;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    //C=Clubs;D=Diamonds;H=Hearts;S=Spades;
    String[] suits = {"C", "D", "H", "S"};

    List<String> deck = getDeck(ranks,suits);


    @SuppressLint("SetTextI18n")

    public void onClick(View view) {
        String card = "";
        int points = 0;
        card = getRandomCard(deck);

        points += getPoints(card);
        TextView text = (TextView) findViewById(R.id.textView1);
        text.setText(card);
        deck.remove(card);

        card = getRandomCard(deck);
        points += getPoints(card);
        TextView thirdText = (TextView) findViewById(R.id.textView3);
        thirdText.setText(card);
        deck.remove(card);


        TextView fiveText = (TextView) findViewById(R.id.textView5);
        fiveText.setText("First player points:" + points);


        points = 0;
        card = getRandomCard(deck);
        points += getPoints(card);

        TextView secondText = (TextView) findViewById(R.id.textView2);
        secondText.setText(card);

        deck.remove(card);
        card = getRandomCard(deck);
        points += getPoints(card);

        TextView forthText = (TextView) findViewById(R.id.textView4);
        forthText.setText(card);

        TextView sixText = (TextView) findViewById(R.id.textView6);
        sixText.setText("Second player points: " + points);
    }

    public List<String> getDeck(String[] ranks, String[] suits) {
        deck=new ArrayList<>();

        for (int i = 0; i < ranks.length; i++) {
            for (int j = 0; j < suits.length; j++) {
                deck.add(ranks[i] + "-" + suits[j]);
            }
        }
        return deck;
    }

    public String getRandomCard(List<String> deck) {
        Random card = new Random();
        int randomNumber = card.nextInt(deck.size());
        return deck.get(randomNumber);
    }


    public int getPoints(String card) {
        String[] input = card.split("-");
        int points = 0;
        switch (input[0]) {
            case "J":
            case "Q":
            case "K":
                points += 10;
                break;
            case "A":
                points += 11;
                break;
            default:
                points += Integer.parseInt(input[0]);
        }
        return points;
    }
}
